CREATE TABLE public.tm_project (
    id character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    status character varying NOT NULL,
    start_date date,
    finish_date date,
    user_id character varying,
    created date
);

CREATE TABLE public.tm_session (
    id character varying(255) NOT NULL,
    "timestamp" bigint NOT NULL,
    signature character varying(255) NOT NULL,
    user_id character varying(255) NOT NULL
);

CREATE TABLE public.tm_task (
    id character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    status character varying NOT NULL,
    project_id character varying(255),
    start_date date,
    finish_date date,
    user_id character varying(255),
    created date
);

CREATE TABLE public.tm_user (
    id character varying(250) NOT NULL,
    login character varying(250) NOT NULL,
    password_hash character varying(250),
    email character varying(250),
    first_name character varying(250),
    last_name character varying(250),
    middle_name character varying(250),
    role character varying(250),
    locked boolean
);