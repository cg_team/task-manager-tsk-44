package ru.inshakov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.TaskAbstractCommand;
import ru.inshakov.tm.util.TerminalUtil;

public class TaskRemoveByIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by id.";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().removeTaskById(getSession(), id);
    }
}
