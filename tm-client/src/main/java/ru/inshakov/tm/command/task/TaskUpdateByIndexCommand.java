package ru.inshakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.TaskAbstractCommand;
import ru.inshakov.tm.endpoint.Task;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.util.TerminalUtil;

public class TaskUpdateByIndexCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Task task = serviceLocator.getTaskEndpoint().findTaskByIndex(getSession(), index);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final Task taskUpdated = serviceLocator.getTaskEndpoint().updateTaskByIndex(getSession(), index, name, description);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }
}
