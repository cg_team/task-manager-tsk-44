package ru.inshakov.tm.exception.empty;

public final class EmptyIdException extends RuntimeException {

    public EmptyIdException() {
        super("Error! Id is empty...");
    }

}
