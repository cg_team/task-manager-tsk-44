package ru.inshakov.tm.api;


import ru.inshakov.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IService<E extends AbstractEntity> {

    List<E> findAll();

    E add(final E entity);

    void addAll(final Collection<E> collection);

    E findById(final String id);

    void clear();

    void removeById(final String id);

    void remove(final E entity);

}