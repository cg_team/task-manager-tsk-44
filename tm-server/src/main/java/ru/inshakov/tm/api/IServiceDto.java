package ru.inshakov.tm.api;


import ru.inshakov.tm.dto.AbstractDtoEntity;

public interface IServiceDto<E extends AbstractDtoEntity> extends IRepositoryDto<E> {

}