package ru.inshakov.tm.api.repository.dto;

import ru.inshakov.tm.dto.ProjectDto;

import java.util.List;

public interface IProjectDtoRepository {

    void add(final ProjectDto project);


    void update(final ProjectDto project);


    ProjectDto findByIdUserId(final String userId, final String id);

    void clearByUserId(final String userId);

    void removeByIdUserId(final String userId, final String id);

    List<ProjectDto> findAllByUserId(final String userId);

    ProjectDto findByName(final String userId, final String name);

    ProjectDto findByIndex(final String userId, final int index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final int index);

    List<ProjectDto> findAll();

    ProjectDto findById(final String id);

    void clear();

    void removeById(final String id);

}
