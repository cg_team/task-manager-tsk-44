package ru.inshakov.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.dto.SessionDto;

import java.util.List;

public interface ISessionDtoRepository {


    List<SessionDto> findAllByUserId(@Nullable String userId);

    void removeByUserId(@Nullable String userId);

    void add(final SessionDto session);

    void update(final SessionDto session);

    List<SessionDto> findAll();

    SessionDto findById(final String id);

    void clear();

    void removeById(final String id);

}
