package ru.inshakov.tm.api.repository.dto;

import ru.inshakov.tm.dto.TaskDto;

import java.util.List;

public interface ITaskDtoRepository {

    List<TaskDto> findAllTaskByProjectId(final String userId, final String projectId);

    void removeAllTaskByProjectId(final String userId, final String projectId);

    void bindTaskToProjectById(final String userId, final String taskId, final String projectId);

    void unbindTaskById(final String userId, final String id);

    void add(final TaskDto task);

    void update(final TaskDto task);

    TaskDto findByIdUserId(final String userId, final String id);

    void clearByUserId(final String userId);

    void removeByIdUserId(final String userId, final String id);

    List<TaskDto> findAllByUserId(final String userId);

    TaskDto findByName(final String userId, final String name);

    TaskDto findByIndex(final String userId, final int index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final int index);

    List<TaskDto> findAll();

    TaskDto findById(final String id);

    void clear();

    void removeById(final String id);

}
