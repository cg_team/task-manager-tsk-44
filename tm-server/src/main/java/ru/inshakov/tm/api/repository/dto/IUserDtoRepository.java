package ru.inshakov.tm.api.repository.dto;

import ru.inshakov.tm.dto.UserDto;

import java.util.List;

public interface IUserDtoRepository {

    UserDto findByLogin(final String login);

    UserDto findByEmail(final String email);

    void removeUserByLogin(final String login);

    void add(final UserDto user);

    void update(final UserDto user);

    List<UserDto> findAll();

    UserDto findById(final String id);

    void clear();

    void removeById(final String id);

}
