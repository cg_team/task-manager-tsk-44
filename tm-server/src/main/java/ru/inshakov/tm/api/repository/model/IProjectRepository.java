package ru.inshakov.tm.api.repository.model;

import ru.inshakov.tm.api.IRepository;
import ru.inshakov.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void update(final Project project);

    Project findByIdUserId(final String userId, final String id);

    void clearByUserId(final String userId);

    void removeByIdUserId(final String userId, final String id);

    List<Project> findAllByUserId(final String userId);

    Project findByName(final String userId, final String name);

    Project findByIndex(final String userId, final int index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final int index);

}
