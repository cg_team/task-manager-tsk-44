package ru.inshakov.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IRepository;
import ru.inshakov.tm.model.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    List<Session> findAllByUserId(@Nullable String userId);

    void removeByUserId(@Nullable String userId);

    void update(final Session session);

}
