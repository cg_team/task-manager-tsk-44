package ru.inshakov.tm.api.repository.model;

import ru.inshakov.tm.api.IRepository;
import ru.inshakov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllTaskByProjectId(final String userId, final String projectId);

    void removeAllTaskByProjectId(final String userId, final String projectId);

    void bindTaskToProjectById(final String userId, final String taskId, final String projectId);

    void unbindTaskById(final String userId, final String id);

    void update(final Task task);

    Task findByIdUserId(final String userId, final String id);

    void clearByUserId(final String userId);

    void removeByIdUserId(final String userId, final String id);

    List<Task> findAllByUserId(final String userId);

    Task findByName(final String userId, final String name);

    Task findByIndex(final String userId, final int index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final int index);


}
