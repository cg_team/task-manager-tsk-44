package ru.inshakov.tm.api.repository.model;

import ru.inshakov.tm.api.IRepository;
import ru.inshakov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(final String login);

    User findByEmail(final String email);

    void removeUserByLogin(final String login);

    void update(final User user);

}
