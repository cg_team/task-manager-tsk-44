package ru.inshakov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IServiceDto;
import ru.inshakov.tm.dto.ProjectDto;

import java.util.Collection;
import java.util.List;

public interface IProjectDtoService extends IServiceDto<ProjectDto> {

    ProjectDto findByName(final String userId, final String name);

    ProjectDto findByIndex(final String userId, final Integer index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final Integer index);

    ProjectDto updateById(final String userId, final String id, final String name, final String description);

    ProjectDto updateByIndex(final String userId, final Integer index, final String name, final String description);

    ProjectDto startById(final String userId, final String id);

    ProjectDto startByIndex(final String userId, final Integer index);

    ProjectDto startByName(final String userId, final String name);

    ProjectDto finishById(final String userId, final String id);

    ProjectDto finishByIndex(final String userId, final Integer index);

    ProjectDto finishByName(final String userId, final String name);

    ProjectDto add(String userId, String name, String description);

    List<ProjectDto> findAll(@NotNull String userId);

    void addAll(String userId, @Nullable Collection<ProjectDto> collection);

    ProjectDto add(String userId, @Nullable ProjectDto entity);

    ProjectDto findById(@NotNull String userId, @Nullable String id);

    void clear(@NotNull String userId);

    void removeById(@NotNull String userId, @Nullable String id);

    void remove(@NotNull String userId, @Nullable ProjectDto entity);
}
