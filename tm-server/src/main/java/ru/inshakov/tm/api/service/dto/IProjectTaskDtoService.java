package ru.inshakov.tm.api.service.dto;

import ru.inshakov.tm.dto.TaskDto;

import java.util.List;

public interface IProjectTaskDtoService {

    List<TaskDto> findTaskByProjectId(String userId, final String projectId);

    void bindTaskById(String userId, final String taskId, final String projectId);

    void unbindTaskById(String userId, final String taskId);

    void removeProjectById(String userId, final String projectId);

    void removeProjectByIndex(String userId, final Integer index);

    void removeProjectByName(String userId, final String name);

}
