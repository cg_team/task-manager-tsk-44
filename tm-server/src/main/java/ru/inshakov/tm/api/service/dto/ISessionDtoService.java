package ru.inshakov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IServiceDto;
import ru.inshakov.tm.dto.SessionDto;
import ru.inshakov.tm.dto.UserDto;
import ru.inshakov.tm.enumerated.Role;

import java.util.List;

public interface ISessionDtoService extends IServiceDto<SessionDto> {

    SessionDto open(@Nullable String login, @Nullable String password);

    UserDto checkDataAccess(@Nullable String login, @Nullable String password);

    void validate(@NotNull SessionDto session, Role role);

    void validate(@Nullable SessionDto session);

    SessionDto sign(@Nullable SessionDto session);

    void close(@Nullable SessionDto session);

    void closeAllByUserId(@Nullable String userId);

    @Nullable List<SessionDto> findAllByUserId(@Nullable String userId);
}
