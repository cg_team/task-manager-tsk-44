package ru.inshakov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IServiceDto;
import ru.inshakov.tm.dto.TaskDto;

import java.util.Collection;
import java.util.List;

public interface ITaskDtoService extends IServiceDto<TaskDto> {

    TaskDto findByName(String userId, String name);

    TaskDto findByIndex(String userId, Integer index);

    void removeByName(String userId, String name);

    void removeByIndex(String userId, Integer index);

    TaskDto updateById(String userId, final String id, final String name, final String description);

    TaskDto updateByIndex(String userId, final Integer index, final String name, final String description);

    TaskDto startById(String userId, String id);

    TaskDto startByIndex(String userId, Integer index);

    TaskDto startByName(String userId, String name);

    TaskDto finishById(String userId, String id);

    TaskDto finishByIndex(String userId, Integer index);

    TaskDto finishByName(String userId, String name);

    TaskDto add(String user, String name, String description);

    List<TaskDto> findAll(@NotNull String userId);

    void addAll(String userId, @Nullable Collection<TaskDto> collection);

    TaskDto add(String user, @Nullable TaskDto entity);

    TaskDto findById(@NotNull String userId, @Nullable String id);

    void clear(@NotNull String userId);

    void removeById(@NotNull String userId, @Nullable String id);

    void remove(@NotNull String userId, @Nullable TaskDto entity);
}
