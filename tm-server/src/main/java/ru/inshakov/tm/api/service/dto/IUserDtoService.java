package ru.inshakov.tm.api.service.dto;

import ru.inshakov.tm.api.IServiceDto;
import ru.inshakov.tm.dto.UserDto;

public interface IUserDtoService extends IServiceDto<UserDto> {

    UserDto findByLogin(final String login);

    UserDto findByEmail(final String email);

    void removeByLogin(final String login);

    UserDto add(final String login, final String password);

    UserDto add(final String login, final String password, final String email);

    UserDto setPassword(final String id, final String password);

    boolean isLoginExist(final String login);

    boolean isEmailExist(final String email);

    UserDto updateUser(
            final String id,
            final String firstName,
            final String lastName,
            final String middleName
    );

    UserDto lockByLogin(final String login);

    UserDto unlockByLogin(final String login);

}
