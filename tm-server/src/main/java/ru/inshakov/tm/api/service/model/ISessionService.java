package ru.inshakov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.model.Session;
import ru.inshakov.tm.model.User;

import java.util.List;

public interface ISessionService extends IService<Session> {

    Session open(@Nullable String login, @Nullable String password);

    User checkDataAccess(@Nullable String login, @Nullable String password);

    void validate(@NotNull Session session, Role role);

    void validate(@Nullable Session session);

    Session sign(@Nullable Session session);

    void close(@Nullable Session session);

    void closeAllByUserId(@Nullable String userId);

    @Nullable List<Session> findAllByUserId(@Nullable String userId);
}
