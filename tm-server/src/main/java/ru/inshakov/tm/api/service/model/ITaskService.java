package ru.inshakov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface ITaskService extends IService<Task> {

    Task findByName(String userId, String name);

    Task findByIndex(String userId, Integer index);

    void removeByName(String userId, String name);

    void removeByIndex(String userId, Integer index);

    Task updateById(String userId, final String id, final String name, final String description);

    Task updateByIndex(String userId, final Integer index, final String name, final String description);

    Task startById(String userId, String id);

    Task startByIndex(String userId, Integer index);

    Task startByName(String userId, String name);

    Task finishById(String userId, String id);

    Task finishByIndex(String userId, Integer index);

    Task finishByName(String userId, String name);

    Task add(User user, String name, String description);

    List<Task> findAll(@NotNull String userId);

    void addAll(User user, @Nullable Collection<Task> collection);

    Task add(User user, @Nullable Task entity);

    Task findById(@NotNull String userId, @Nullable String id);

    void clear(@NotNull String userId);

    void removeById(@NotNull String userId, @Nullable String id);

    void remove(@NotNull String userId, @Nullable Task entity);
}
