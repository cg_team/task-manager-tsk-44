package ru.inshakov.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.service.*;
import ru.inshakov.tm.api.service.dto.*;
import ru.inshakov.tm.api.service.model.*;
import ru.inshakov.tm.dto.ProjectDto;
import ru.inshakov.tm.dto.UserDto;
import ru.inshakov.tm.endpoint.*;
import ru.inshakov.tm.model.User;
import ru.inshakov.tm.service.*;
import ru.inshakov.tm.service.dto.*;
import ru.inshakov.tm.service.model.ProjectService;
import ru.inshakov.tm.service.model.SessionService;
import ru.inshakov.tm.service.model.TaskService;
import ru.inshakov.tm.service.model.UserService;
import ru.inshakov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@Getter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskDtoService taskDtoService = new TaskDtoService(connectionService);

    @NotNull
    private final IProjectDtoService projectDtoService = new ProjectDtoService(connectionService);

    @NotNull
    private final IProjectTaskDtoService projectTaskDtoService = new ProjectTaskDtoService(connectionService);

    @NotNull
    private final IUserDtoService userDtoService = new UserDtoService(connectionService, propertyService);

    @NotNull
    private final ISessionDtoService sessionDtoService = new SessionDtoService(connectionService, userDtoService, propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService, userService, propertyService);

    @NotNull
    private final DataService dataService = new DataService(userDtoService, taskDtoService, projectDtoService, sessionDtoService);

    @NotNull
    private final ILoggerService logService = new LoggerService();


    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this, projectDtoService, projectTaskDtoService, projectService);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this, sessionDtoService, userDtoService, sessionService);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this, taskDtoService, projectTaskDtoService, taskService);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpoint(this, userDtoService, userService, sessionDtoService);

    @NotNull
    private final DataEndpoint dataEndpoint = new DataEndpoint(this, dataService);

    public void init() {
        initPID();
        initEndpoints();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void registry(final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final String port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl,endpoint);
    }

    @SneakyThrows
    private void initEndpoints(){
        @NotNull final Reflections reflections = new Reflections("ru.inshakov.tm.endpoint");
        @NotNull final Set<Class<? extends AbstractEndpoint>> classes =
                reflections.getSubTypesOf(AbstractEndpoint.class);
        for (@NotNull final Class<? extends AbstractEndpoint> clazz:classes){
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

}
