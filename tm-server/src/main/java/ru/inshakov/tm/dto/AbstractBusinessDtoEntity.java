package ru.inshakov.tm.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Setter
@Getter
@MappedSuperclass
public abstract class AbstractBusinessDtoEntity extends AbstractDtoEntity {

    @Column(name = "user_id")
    protected String userId;

}
