package ru.inshakov.tm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@JsonRootName("domain")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DomainDto implements Serializable {

    @Nullable
    private String date = new Date().toString();

    @Nullable
    @JsonProperty("user")
    @JacksonXmlElementWrapper(localName = "users")
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    private List<UserDto> users;

    @Nullable
    @JsonProperty("project")
    @JacksonXmlElementWrapper(localName = "projects")
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    private List<ProjectDto> projects;

    @Nullable
    @JsonProperty("task")
    @JacksonXmlElementWrapper(localName = "tasks")
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    private List<TaskDto> tasks;

}
