package ru.inshakov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "tm_task")
@NoArgsConstructor
public class TaskDto extends AbstractBusinessDtoEntity {

    @Column
    @NotNull
    private String name;

    @Column
    @Nullable
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Column(name = "project_id")
    private String projectId;

    @Nullable
    @Column(name = "start_date")
    private Date startDate;

    @Nullable
    @Column(name = "finish_date")
    private Date finishDate;

    @Column
    @NotNull
    private Date created = new Date();

    @NotNull
    public TaskDto(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public TaskDto(@NotNull String name, @Nullable String description) {
        this.name = name;
        this.description = description;
    }


}
