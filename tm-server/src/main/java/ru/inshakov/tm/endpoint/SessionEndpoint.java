package ru.inshakov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.api.service.ServiceLocator;
import ru.inshakov.tm.api.service.dto.ISessionDtoService;
import ru.inshakov.tm.api.service.dto.IUserDtoService;
import ru.inshakov.tm.api.service.model.ISessionService;
import ru.inshakov.tm.api.service.model.IUserService;
import ru.inshakov.tm.dto.SessionDto;
import ru.inshakov.tm.dto.UserDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public final class SessionEndpoint extends AbstractEndpoint {

    private ISessionDtoService sessionDtoService;

    private IUserDtoService userService;

    private ISessionService sessionService;

    public SessionEndpoint(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final ISessionDtoService sessionDtoService,
            @NotNull final IUserDtoService userService,
            @NotNull final ISessionService sessionService
    ) {
        super(serviceLocator);
        this.sessionDtoService = sessionDtoService;
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public SessionDto open(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password
    ) {

        SessionDto session = sessionDtoService.open(login, password);
        return session;
    }

    @WebMethod
    public void close(@WebParam(name = "session") final SessionDto session) {
        serviceLocator.getSessionDtoService().validate(session);
        sessionDtoService.close(session);
    }

    @WebMethod
    public SessionDto register(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password,
            @WebParam(name = "email") final String email
    ) {
        userService.add(login, password, email);
        return sessionDtoService.open(login, password);
    }

    @WebMethod
    public UserDto setPassword(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "password") final String password
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return userService.setPassword(session.getUserId(), password);
    }

    @WebMethod
    public UserDto updateUser(
            @WebParam(name = "session") final SessionDto session,
            @WebParam(name = "firstName") final String firstName,
            @WebParam(name = "lastName") final String lastName,
            @WebParam(name = "middleName") final String middleName
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return userService.updateUser(session.getUserId(), firstName, lastName, middleName);
    }

}
