package ru.inshakov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.api.service.ServiceLocator;
import ru.inshakov.tm.api.service.dto.IProjectTaskDtoService;
import ru.inshakov.tm.api.service.dto.ITaskDtoService;
import ru.inshakov.tm.api.service.model.ITaskService;
import ru.inshakov.tm.dto.SessionDto;
import ru.inshakov.tm.dto.TaskDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@NoArgsConstructor
public final class TaskEndpoint extends AbstractEndpoint {

    private ITaskDtoService taskDtoService;

    private IProjectTaskDtoService projectTaskService;

    private ITaskService taskService;

    public TaskEndpoint(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final ITaskDtoService taskDtoService,
            @NotNull final IProjectTaskDtoService projectTaskService,
            @NotNull final ITaskService taskService
    ) {
        super(serviceLocator);
        this.taskDtoService = taskDtoService;
        this.projectTaskService = projectTaskService;
        this.taskService = taskService;
    }

    @WebMethod
    public List<TaskDto> findTaskAll(@NotNull @WebParam(name = "session") final SessionDto session) {
        serviceLocator.getSessionDtoService().validate(session);
        return taskDtoService.findAll(session.getUserId());
    }

    @WebMethod
    public void addTaskAll(
            @WebParam(name = "session") final SessionDto session,
            @WebParam(name = "collection") final Collection<TaskDto> collection
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        taskDtoService.addAll(session.getUserId(), collection);
    }

    @WebMethod
    public TaskDto addTask(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "task") final TaskDto entity
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return taskDtoService.add(session.getUserId(), entity);
    }

    @WebMethod
    public TaskDto addTaskWithName(
            @WebParam(name = "session") final SessionDto session,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return taskDtoService.add(session.getUserId(), name, description);
    }

    @WebMethod
    public TaskDto findTaskById(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return taskDtoService.findById(session.getUserId(), id);
    }

    @WebMethod
    public void clearTask(@WebParam(name = "session") final SessionDto session) {
        serviceLocator.getSessionDtoService().validate(session);
        taskService.clear(session.getUserId());
    }

    @WebMethod
    public void removeTaskById(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        taskService.removeById(session.getUserId(), id);
    }

    @WebMethod
    public void removeTask(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "task") final TaskDto entity
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        taskDtoService.remove(session.getUserId(), entity);
    }

    @WebMethod
    public TaskDto findTaskByName(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return taskDtoService.findByName(session.getUserId(), name);
    }

    @WebMethod
    public TaskDto findTaskByIndex(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return taskDtoService.findByIndex(session.getUserId(), index);
    }

    @WebMethod
    public void removeTaskByName(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        taskService.removeByName(session.getUserId(), name);
    }

    @WebMethod
    public void removeTaskByIndex(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        taskService.removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    public TaskDto updateTaskById(
            @WebParam(name = "session") final SessionDto session,
            @WebParam(name = "id") final String id,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return taskDtoService.updateById(session.getUserId(), id, name, description);
    }

    @WebMethod
    public TaskDto updateTaskByIndex(
            @WebParam(name = "session") final SessionDto session,
            @WebParam(name = "index") final Integer index,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return taskDtoService.updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public TaskDto startTaskById(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return taskDtoService.startById(session.getUserId(), id);
    }

    @WebMethod
    public TaskDto startTaskByIndex(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return taskDtoService.startByIndex(session.getUserId(), index);
    }

    @WebMethod
    public TaskDto startTaskByName(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return taskDtoService.startByName(session.getUserId(), name);
    }

    @WebMethod
    public TaskDto finishTaskById(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return taskDtoService.finishById(session.getUserId(), id);
    }

    @WebMethod
    public TaskDto finishTaskByIndex(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return taskDtoService.finishByIndex(session.getUserId(), index);
    }

    @WebMethod
    public TaskDto finishTaskByName(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return taskDtoService.finishByName(session.getUserId(), name);
    }

    @WebMethod
    public List<TaskDto> findTaskByProjectId(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return projectTaskService.findTaskByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    public void bindTaskById(
            @WebParam(name = "session") final SessionDto session,
            @WebParam(name = "taskId") final String taskId,
            @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        projectTaskService.bindTaskById(session.getUserId(), taskId, projectId);
    }

    @WebMethod
    public void unbindTaskById(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "taskId") final String taskId
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        projectTaskService.unbindTaskById(session.getUserId(), taskId);
    }

}
