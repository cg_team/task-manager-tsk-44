package ru.inshakov.tm.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
@AllArgsConstructor
public enum Role {

    ADMIN("Administrator"),

    USER("UserDto");

    @NotNull
    private String displayName;

}

