package ru.inshakov.tm.exception.empty;

public final class EmptyDomainException extends RuntimeException {

    public EmptyDomainException() {
        super("Error! DomainDto is empty...");
    }

}
