package ru.inshakov.tm.exception.entity;

public final class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException() {
        super("Error! Entity not found...");
    }

}
