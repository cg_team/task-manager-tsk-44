package ru.inshakov.tm.exception.entity;

public final class ProjectNotFoundException extends RuntimeException {

    public ProjectNotFoundException() {
        super("Error! ProjectDto not found...");
    }

}
