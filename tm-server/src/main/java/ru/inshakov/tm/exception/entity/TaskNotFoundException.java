package ru.inshakov.tm.exception.entity;

public final class TaskNotFoundException extends RuntimeException {

    public TaskNotFoundException() {
        super("Error! TaskDto not found...");
    }

}
