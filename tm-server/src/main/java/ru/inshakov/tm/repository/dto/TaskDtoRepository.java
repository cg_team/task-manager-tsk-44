package ru.inshakov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.dto.ITaskDtoRepository;
import ru.inshakov.tm.dto.TaskDto;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskDtoRepository extends AbstractDtoRepository<TaskDto> implements ITaskDtoRepository {

    public TaskDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    public TaskDto findById(@Nullable final String id) {
        return entityManager.find(TaskDto.class, id);
    }

    public void remove(final TaskDto entity) {
        TaskDto reference = entityManager.getReference(TaskDto.class, entity.getId());
        entityManager.remove(reference);
    }

    public void removeById(@Nullable final String id) {
        TaskDto reference = entityManager.getReference(TaskDto.class, id);
        entityManager.remove(reference);
    }

    @NotNull
    public List<TaskDto> findAll() {
        return entityManager.createQuery("SELECT e FROM TaskDto e", TaskDto.class).getResultList();
    }

    @Override
    public List<TaskDto> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM TaskDto e WHERE e.userId = :userId", TaskDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public List<TaskDto> findAllTaskByProjectId(String userId, String projectId) {
        return entityManager
                .createQuery(
                        "SELECT e FROM TaskDto e WHERE e.userId = :userId AND e.projectId = :projectId", TaskDto.class
                )
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeAllTaskByProjectId(String userId, String projectId) {
        entityManager
                // .createQuery("DELETE FROM TaskDto e WHERE e.userId = :userId AND e.projectId = :projectId")
                .createQuery("DELETE FROM TaskDto e WHERE e.userId = :userId AND e.projectId = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void bindTaskToProjectById(String userId, String taskId, String projectId) {
        entityManager
                .createQuery("UPDATE TaskDto e SET e.projectId = :projectId WHERE e.userId = :userId AND e.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void unbindTaskById(String userId, String id) {
        entityManager
                .createQuery("UPDATE TaskDto e SET e.projectId = NULL WHERE e.userId = :userId AND e.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public TaskDto findByIdUserId(String userId, String id) {
        List<TaskDto> list = entityManager
                .createQuery("SELECT e FROM TaskDto e WHERE e.id = :id AND e.userId = :userId", TaskDto.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }


    @Nullable
    @Override
    public TaskDto findByName(@NotNull final String userId, @Nullable final String name) {
        List<TaskDto> list = entityManager
                .createQuery("SELECT e FROM TaskDto e WHERE e.name = :name AND e.userId = :userId", TaskDto.class)
                .setParameter("name", name)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Nullable
    @Override
    public TaskDto findByIndex(@NotNull final String userId, final int index) {
        List<TaskDto> list = entityManager
                .createQuery("SELECT e FROM TaskDto e WHERE e.userId = :userId", TaskDto.class)
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Override
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM TaskDto e WHERE e.name = :name AND e.userId = :userId")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM TaskDto e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM TaskDto e")
                .executeUpdate();
    }

    @Override
    public void clearByUserId(String userId) {
        entityManager
                .createQuery("DELETE FROM TaskDto e WHERE e.userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeByIdUserId(String userId, String id) {
        entityManager
                .createQuery("DELETE FROM TaskDto e WHERE e.userId = :userId AND e.id =:id")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}