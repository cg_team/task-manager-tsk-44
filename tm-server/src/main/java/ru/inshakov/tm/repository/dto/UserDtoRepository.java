package ru.inshakov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.dto.IUserDtoRepository;
import ru.inshakov.tm.dto.UserDto;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserDtoRepository extends AbstractDtoRepository<UserDto> implements IUserDtoRepository {

    public UserDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    public List<UserDto> findAll() {
        return entityManager.createQuery("SELECT e FROM UserDto e", UserDto.class).getResultList();
    }

    public UserDto findById(@Nullable final String id) {
        return entityManager.find(UserDto.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM UserDto e")
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        UserDto reference = entityManager.getReference(UserDto.class, id);
        entityManager.remove(reference);
    }

    @Nullable
    @Override
    public UserDto findByLogin(@Nullable final String login) {
        List<UserDto> list = entityManager
                .createQuery("SELECT e FROM UserDto e WHERE e.login = :login", UserDto.class)
                .setParameter("login", login)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Nullable
    @Override
    public UserDto findByEmail(@Nullable final String email) {
        List<UserDto> list = entityManager
                .createQuery("SELECT e FROM UserDto e WHERE e.email = :email", UserDto.class)
                .setParameter("email", email)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Override
    public void removeUserByLogin(@Nullable final String login) {
        entityManager
                .createQuery("DELETE FROM UserDto e WHERE e.login = :login")
                .setParameter(login, login)
                .executeUpdate();
    }

}