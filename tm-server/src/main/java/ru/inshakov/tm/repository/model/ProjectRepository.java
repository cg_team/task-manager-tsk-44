package ru.inshakov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.model.IProjectRepository;
import ru.inshakov.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    public Project getReference(@NotNull final String id) {
        return entityManager.getReference(Project.class, id);
    }

    public Project findById(@Nullable final String id) {
        return entityManager.find(Project.class, id);
    }

    public void removeById(@Nullable final String id) {
        Project reference = entityManager.getReference(Project.class, id);
        entityManager.remove(reference);
    }

    @NotNull
    public List<Project> findAll() {
        return entityManager.createQuery("SELECT e FROM Project e", Project.class).getResultList();
    }

    @Override
    public List<Project> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM Project e WHERE e.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public Project findByIdUserId(String userId, String id) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM Project e WHERE e.id = :id AND e.user.id = :userId", Project.class)
                        .setParameter("id", id)
                        .setParameter("userId", userId)
                        .setMaxResults(1)
        );
    }


    @Nullable
    @Override
    public Project findByName(@NotNull final String userId, @Nullable final String name) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM Project e WHERE e.name = :name AND e.user.id = :userId", Project.class)
                        .setParameter("name", name)
                        .setParameter("userId", userId)
                        .setMaxResults(1)
        );
    }

    @Nullable
    @Override
    public Project findByIndex(@NotNull final String userId, final int index) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM Project e WHERE e.user.id = :userId", Project.class)
                        .setParameter("userId", userId)
                        .setFirstResult(index).setMaxResults(1)
        );
    }

    @Override
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM Project e WHERE e.name = :name AND e.user.id = :userId")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM Project e WHERE e.user.id = :userId")
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM Project e")
                .executeUpdate();
    }

    @Override
    public void clearByUserId(String userId) {
        entityManager
                .createQuery("DELETE FROM Project e WHERE e.user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeByIdUserId(String userId, String id) {
        entityManager
                .createQuery("DELETE FROM Project e WHERE e.user.id = :userId AND e.id = :id")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}