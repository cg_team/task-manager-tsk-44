package ru.inshakov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.model.ISessionRepository;
import ru.inshakov.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    public Session getReference(@NotNull final String id) {
        return entityManager.getReference(Session.class, id);
    }

    @Override
    public List<Session> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM Session e WHERE e.user.id = :userId", Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void removeByUserId(String userId) {
        entityManager
                .createQuery("DELETE FROM Session e WHERE e.user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @NotNull
    public List<Session> findAll() {
        return entityManager.createQuery("SELECT e FROM Session e", Session.class).getResultList();
    }

    public Session findById(@Nullable final String id) {
        return entityManager.find(Session.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM Session e")
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        Session reference = entityManager.getReference(Session.class, id);
        entityManager.remove(reference);
    }
}