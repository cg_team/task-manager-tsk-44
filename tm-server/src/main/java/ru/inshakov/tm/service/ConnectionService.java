package ru.inshakov.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.dto.ProjectDto;
import ru.inshakov.tm.dto.SessionDto;
import ru.inshakov.tm.dto.TaskDto;
import ru.inshakov.tm.dto.UserDto;
import ru.inshakov.tm.exception.system.DatabaseInitException;
import ru.inshakov.tm.model.Project;
import ru.inshakov.tm.model.Session;
import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        entityManagerFactory = factory();
    }

    @Override
    public @NotNull EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    private EntityManagerFactory factory() {
        @Nullable final String driver = propertyService.getJdbcDriver();
        if (driver == null) throw new DatabaseInitException();
        @Nullable final String username = propertyService.getJdbcUser();
        if (username == null) throw new DatabaseInitException();
        @Nullable final String password = propertyService.getJdbcPassword();
        if (password == null) throw new DatabaseInitException();
        @Nullable final String url = propertyService.getJdbcUrl();
        if (url == null) throw new DatabaseInitException();
        @Nullable final String dialect = propertyService.getHibernateDialect();
        if (dialect == null) throw new DatabaseInitException();
        @Nullable final String auto = propertyService.getHibernateBM2DDLAuto();
        if (auto == null) throw new DatabaseInitException();
        @Nullable final String sqlShow = propertyService.getHibernateShowSql();
        if (sqlShow == null) throw new DatabaseInitException();

        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, driver);
        settings.put(org.hibernate.cfg.Environment.URL, url);
        settings.put(org.hibernate.cfg.Environment.USER, username);
        settings.put(org.hibernate.cfg.Environment.PASS, password);
        settings.put(org.hibernate.cfg.Environment.DIALECT, dialect);
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, auto);
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, sqlShow);

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);

        sources.addAnnotatedClass(ProjectDto.class);
        sources.addAnnotatedClass(TaskDto.class);
        sources.addAnnotatedClass(SessionDto.class);
        sources.addAnnotatedClass(UserDto.class);

        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(User.class);

        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
