package ru.inshakov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.other.ISignatureSetting;

import java.io.InputStream;
import java.util.Properties;

import static ru.inshakov.tm.constant.PropertyConst.*;

public final class PropertyService implements IPropertyService, ISignatureSetting {

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        if (System.getenv().containsKey(PASSWORD_SECRET_KEY))
            return System.getenv(PASSWORD_SECRET_KEY);
        if (System.getProperties().containsKey(PASSWORD_SECRET_KEY))
            return System.getProperty(PASSWORD_SECRET_KEY);
        return properties.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_VALUE);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        if (System.getenv().containsKey(PASSWORD_ITERATION_KEY)) {
            @NotNull final String value = System.getenv(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        if (System.getProperties().containsKey(PASSWORD_ITERATION_KEY)) {
            @NotNull final String value = System.getProperty(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        @NotNull final String value = properties.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_VALUE);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        if (System.getenv().containsKey(APPLICATION_VERSION_KEY))
            return System.getenv(APPLICATION_VERSION_KEY);
        if (System.getProperties().containsKey(APPLICATION_VERSION_KEY))
            return System.getProperty(APPLICATION_VERSION_KEY);
        return properties.getProperty(APPLICATION_VERSION_KEY, APPLICATION_VERSION_VALUE);
    }

    @Override
    @NotNull
    public String getServerHost() {
        if (System.getenv().containsKey(SERVER_HOST_KEY))
            return System.getenv(SERVER_HOST_KEY);
        if (System.getProperties().containsKey(SERVER_HOST_KEY))
            return System.getProperty(SERVER_HOST_KEY);
        return properties.getProperty(SERVER_HOST_KEY, SERVER_HOST_VALUE);
    }

    @Override
    @NotNull
    public String getServerPort() {
        if (System.getenv().containsKey(SERVER_PORT_KEY))
            return System.getenv(SERVER_PORT_KEY);
        if (System.getProperties().containsKey(SERVER_PORT_KEY))
            return System.getProperty(SERVER_PORT_KEY);
        return properties.getProperty(SERVER_PORT_KEY, SERVER_PORT_VALUE);
    }

    @NotNull
    @Override
    public String getSignatureSecret() {
        if (System.getenv().containsKey(SIGNATURE_SECRET_KEY))
            return System.getenv(SIGNATURE_SECRET_KEY);
        if (System.getProperties().containsKey(SIGNATURE_SECRET_KEY))
            return System.getProperty(SIGNATURE_SECRET_KEY);
        return properties.getProperty(SIGNATURE_SECRET_KEY, SIGNATURE_SECRET_VALUE);
    }

    @NotNull
    @Override
    public Integer getSignatureIteration() {
        if (System.getenv().containsKey(SIGNATURE_ITERATION_KEY)) {
            @NotNull final String value = System.getenv(SIGNATURE_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        if (System.getProperties().containsKey(SIGNATURE_ITERATION_KEY)) {
            @NotNull final String value = System.getProperty(SIGNATURE_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        @NotNull final String value = properties.getProperty(SIGNATURE_ITERATION_KEY, SIGNATURE_ITERATION_VALUE);
        return Integer.parseInt(value);
    }

    @Override
    @Nullable
    public String getJdbcUser() {
        if (System.getenv().containsKey(JDBC_USER_KEY)) {
            @NotNull final String value = System.getenv(JDBC_USER_KEY);
            return value;
        }
        if (System.getProperties().containsKey(JDBC_USER_KEY)) {
            @NotNull final String value = System.getProperty(JDBC_USER_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(JDBC_USER_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getJdbcPassword() {
        if (System.getenv().containsKey(JDBC_PASSWORD_KEY)) {
            @NotNull final String value = System.getenv(JDBC_PASSWORD_KEY);
            return value;
        }
        if (System.getProperties().containsKey(JDBC_PASSWORD_KEY)) {
            @NotNull final String value = System.getProperty(JDBC_PASSWORD_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(JDBC_PASSWORD_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getJdbcUrl() {
        if (System.getenv().containsKey(JDBC_URL_KEY)) {
            @NotNull final String value = System.getenv(JDBC_URL_KEY);
            return value;
        }
        if (System.getProperties().containsKey(JDBC_URL_KEY)) {
            @NotNull final String value = System.getProperty(JDBC_URL_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(JDBC_URL_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getJdbcDriver() {
        if (System.getenv().containsKey(JDBC_DRIVER_KEY)) {
            @NotNull final String value = System.getenv(JDBC_DRIVER_KEY);
            return value;
        }
        if (System.getProperties().containsKey(JDBC_DRIVER_KEY)) {
            @NotNull final String value = System.getProperty(JDBC_DRIVER_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(JDBC_DRIVER_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getHibernateDialect() {
        if (System.getenv().containsKey(HIBERNATE_DIALECT_KEY)) {
            @NotNull final String value = System.getenv(HIBERNATE_DIALECT_KEY);
            return value;
        }
        if (System.getProperties().containsKey(HIBERNATE_DIALECT_KEY)) {
            @NotNull final String value = System.getProperty(HIBERNATE_DIALECT_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(HIBERNATE_DIALECT_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getHibernateBM2DDLAuto() {
        if (System.getenv().containsKey(HIBERNATE_HBM2DDL_AUTO_KEY)) {
            @NotNull final String value = System.getenv(HIBERNATE_HBM2DDL_AUTO_KEY);
            return value;
        }
        if (System.getProperties().containsKey(HIBERNATE_HBM2DDL_AUTO_KEY)) {
            @NotNull final String value = System.getProperty(HIBERNATE_HBM2DDL_AUTO_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(HIBERNATE_HBM2DDL_AUTO_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getHibernateShowSql() {
        if (System.getenv().containsKey(HIBERNATE_SHOW_SQL_KEY)) {
            @NotNull final String value = System.getenv(HIBERNATE_SHOW_SQL_KEY);
            return value;
        }
        if (System.getProperties().containsKey(HIBERNATE_SHOW_SQL_KEY)) {
            @NotNull final String value = System.getProperty(HIBERNATE_SHOW_SQL_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(HIBERNATE_SHOW_SQL_KEY);
        return value;
    }

}
