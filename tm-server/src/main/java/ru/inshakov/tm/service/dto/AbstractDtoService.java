package ru.inshakov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.api.IServiceDto;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.dto.AbstractDtoEntity;

public abstract class AbstractDtoService<E extends AbstractDtoEntity> implements IServiceDto<E> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public AbstractDtoService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
