package ru.inshakov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.repository.dto.ISessionDtoRepository;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.api.service.dto.ISessionDtoService;
import ru.inshakov.tm.api.service.dto.IUserDtoService;
import ru.inshakov.tm.dto.SessionDto;
import ru.inshakov.tm.dto.UserDto;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.system.AccessDeniedException;
import ru.inshakov.tm.repository.dto.SessionDtoRepository;
import ru.inshakov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public final class SessionDtoService extends AbstractDtoService<SessionDto> implements ISessionDtoService {

    @NotNull
    private final IUserDtoService userService;

    @NotNull
    private final IPropertyService propertyService;

    public SessionDtoService(
            @NotNull final IConnectionService connectionService,
            @NotNull IUserDtoService userService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionDto> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<SessionDto> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (SessionDto item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDto add(@Nullable final SessionDto entity) {
        if (entity == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            repository.add(entity);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDto findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            repository.removeById(optionalId.orElseThrow(EmptyIdException::new));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final SessionDto entity) {
        if (entity == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            repository.removeById(entity.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    @NotNull
    public SessionDto open(@Nullable final String login, @Nullable final String password) {
        @Nullable final UserDto user = checkDataAccess(login, password);
        if (user == null) throw new AccessDeniedException();
        final SessionDto session = new SessionDto();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        @Nullable final SessionDto resultSession = sign(session);
        add(resultSession);
        return resultSession;
    }

    @Override
    @SneakyThrows
    public UserDto checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable final UserDto user = userService.findByLogin(login);
        if (user == null) return null;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null || hash.isEmpty() || user.isLocked()) return null;
        if (hash.equals(user.getPasswordHash())) {
            return user;
        } else {
            return null;
        }
    }

    @Override
    @SneakyThrows
    public void validate(@NotNull final SessionDto session, final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDto user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionDto session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionDto temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            if (repository.findById(session.getId()) == null) throw new AccessDeniedException();
        } finally {
            entityManager.close();
        }

    }

    @Override
    @SneakyThrows
    @Nullable
    public SessionDto sign(@Nullable final SessionDto session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.sign(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    @SneakyThrows
    public void close(@Nullable final SessionDto session) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            repository.removeById(session.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void closeAllByUserId(@Nullable final String userId) {
        if (userId == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            repository.removeByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    @Nullable
    public List<SessionDto> findAllByUserId(@Nullable final String userId) {
        if (userId == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            return repository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }
}
