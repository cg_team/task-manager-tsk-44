package ru.inshakov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.repository.dto.IUserDtoRepository;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.api.service.dto.IUserDtoService;
import ru.inshakov.tm.dto.UserDto;
import ru.inshakov.tm.exception.empty.*;
import ru.inshakov.tm.exception.entity.UserNotFoundException;
import ru.inshakov.tm.exception.user.EmailTakenException;
import ru.inshakov.tm.exception.user.LoginTakenException;
import ru.inshakov.tm.repository.dto.UserDtoRepository;
import ru.inshakov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public final class UserDtoService extends AbstractDtoService<UserDto> implements IUserDtoService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    public UserDtoService(
            @NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<UserDto> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = new UserDtoRepository(entityManager);
            return repository.findAll();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<UserDto> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (UserDto item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDto add(@Nullable final UserDto entity) {
        if (entity == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IUserDtoRepository repository = new UserDtoRepository(entityManager);
            repository.add(entity);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    private void update(@NotNull final UserDto entity, @NotNull EntityManager entityManager) {
        @NotNull final IUserDtoRepository repository = new UserDtoRepository(entityManager);
        repository.update(entity);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDto findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = new UserDtoRepository(entityManager);
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = new UserDtoRepository(entityManager);
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = new UserDtoRepository(entityManager);
            repository.removeById(optionalId.orElseThrow(EmptyIdException::new));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final UserDto entity) {
        if (entity == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = new UserDtoRepository(entityManager);
            repository.removeById(entity.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDto findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            return userRepository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public UserDto findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            return userRepository.findByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            userRepository.removeUserByLogin(login);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDto add(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExist(login)) throw new LoginTakenException(login);
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        @NotNull final String passwordHash = Optional.ofNullable(HashUtil.salt(propertyService, password))
                .orElseThrow(EmptyPasswordException::new);
        user.setPasswordHash(passwordHash);
        add(user);

        return user;
    }

    @Override
    @SneakyThrows
    public UserDto add(
            @Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExist(login)) throw new LoginTakenException(login);
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isEmailExist(email)) throw new EmailTakenException(login);
        final UserDto user = new UserDto();
        user.setLogin(login);
        user.setEmail(email);
        @NotNull final String passwordHash = Optional.ofNullable(HashUtil.salt(propertyService, password))
                .orElseThrow(EmptyPasswordException::new);
        user.setPasswordHash(passwordHash);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDto setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final UserDto user = findById(id);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String passwordHash = Optional.ofNullable(HashUtil.salt(propertyService, password))
                .orElseThrow(EmptyPasswordException::new);
        user.setPasswordHash(passwordHash);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            update(user, entityManager);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }


    @Override
    @SneakyThrows
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDto updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final UserDto user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            update(user, entityManager);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDto lockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            update(user, entityManager);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDto unlockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            update(user, entityManager);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
