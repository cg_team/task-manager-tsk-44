package ru.inshakov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.model.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
